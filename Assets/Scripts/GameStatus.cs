using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStatus : MonoBehaviour
{
    private static GameStatus m_Instance;
    public static GameStatus Instance
    {
        get => m_Instance;
    }


    [field: SerializeField]
    public int Level { get; set; } = 1;
    [field: SerializeField]
    public GameMode Mode { get; set; } = GameMode.SinglePlayer;

    public int CurrentScore { get; set; } = 0;
    public int HighScore { get; set; } = 0;

    private void Awake()
    {
        if (m_Instance != null && m_Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
}
