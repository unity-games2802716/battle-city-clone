using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    const float DefaultIndicatorSize = 100f;
    
    [field: SerializeField]
    public bool Interactable { get; set; } = true;
    [SerializeField]
    private Image m_Indicator;
    [SerializeField]
    private List<MenuButton> m_MenuItems = new();

    private int m_CurrentMenuIndex = 0;
    private float m_IndicatorSize;

    private void Start()
    {   
        RectTransform indicatorRectTransform = m_Indicator.GetComponent<RectTransform>();
        m_IndicatorSize = m_Indicator ? indicatorRectTransform.rect.width : DefaultIndicatorSize;
        m_CurrentMenuIndex = m_MenuItems.FindIndex(menuItem => {
            return menuItem.interactable;
        });

        UnselectMenuItems();
        
        if (Interactable && IsAnyButtonInteractable())
        {
            HighlightCurrentButton();
            MoveIndicator();
        }
    }

    private void Update() {
        if (!Interactable || !IsAnyButtonInteractable())
        {
            m_Indicator.enabled = false;
            return;
        }
        else
        {
            m_Indicator.enabled = true;
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            ResolvePrevButton();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            ResolveNextButton();
        }
        else if (Input.GetKeyDown(KeyCode.Return))
        {
            m_MenuItems[m_CurrentMenuIndex].OnMenuSubmit.Invoke();
        }

        UnselectMenuItems();
        HighlightCurrentButton();
        MoveIndicator();
    }

    private bool IsAnyButtonInteractable()
    {
        return m_MenuItems.Where(menuItem => !menuItem.interactable).Count() < m_MenuItems.Count;
    }

    private void ResolveNextButton()
    {   
        int loopedThrough = 0;
        
        do
        {
            if (loopedThrough >= m_MenuItems.Count)
            {
                break;
            }

            m_CurrentMenuIndex = m_CurrentMenuIndex < (m_MenuItems.Count - 1)
                ? (m_CurrentMenuIndex + 1) % m_MenuItems.Count
                : 0;   
            loopedThrough++;
        } while (!m_MenuItems[m_CurrentMenuIndex].interactable);
    }

    private void ResolvePrevButton()
    {
        int loopedThrough = 0;
        
        do
        {
            if (loopedThrough >= m_MenuItems.Count)
            {
                break;
            }

            m_CurrentMenuIndex = m_CurrentMenuIndex != 0
                ? (m_CurrentMenuIndex - 1) % m_MenuItems.Count
                : (m_MenuItems.Count - 1);
            loopedThrough++;
        } while (!m_MenuItems[m_CurrentMenuIndex].interactable);
    }

    private void HighlightCurrentButton()
    {
        m_MenuItems[m_CurrentMenuIndex].highlight = true;
    }

    private void MoveIndicator()
    {
        if (m_CurrentMenuIndex >= 0)
        {
            RectTransform indicatorRect = m_Indicator.GetComponent<RectTransform>();
            indicatorRect.anchoredPosition = new Vector2(
                indicatorRect.anchoredPosition.x,
                m_IndicatorSize - m_CurrentMenuIndex * m_IndicatorSize
            );
            // m_Indicator.transform.position = new Vector3(
            //     m_Indicator.transform.position.x,
            //     50 - m_CurrentMenuIndex * IndicatorSize,
            //     m_Indicator.transform.position.z
            // );
            
        }
    }

    private void UnselectMenuItems() 
    {
        m_MenuItems.ForEach(menuItem => {
            menuItem.highlight = false;
        });
    }
}
