using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StageSplash : MonoBehaviour
{
    [SerializeField]
    private TMP_Text m_TitleElement;

    // Start is called before the first frame update
    void Start()
    {
        

        if (!m_TitleElement)
        {
            throw new Exception("Missing a title element!");
        }
        
        m_TitleElement.text = $"Stage {GameStatus.Instance.Level}";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
