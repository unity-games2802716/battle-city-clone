using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuActions : MonoBehaviour
{
    public void StartGame1P()
    {   
        StartGame(GameMode.SinglePlayer);
    }

    public void StartGame2P()
    {
        StartGame(GameMode.MultiPlayer);
    }

    public void QuitQame()
    {
        Application.Quit();
    }

    private void StartGame(GameMode mode)
    {
        GameStatus.Instance.Mode = mode;
        SceneManager.LoadScene((int) GameScene.StageSplash);
    }
}
