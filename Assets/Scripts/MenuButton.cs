using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MenuButton : MonoBehaviour
{
    public bool highlight { get; set; } = false;
    public bool interactable 
    {
        get => m_Button.interactable;
        set => m_Button.interactable = value;
    }
    
    [SerializeField]
    private Color m_NormalColor = Color.gray;
    [SerializeField]
    private Color m_HighlightColor = Color.white;
    [SerializeField]
    private Color m_DisabledColor = new(0.5f, 0.5f, 0.5f, 0.15f);

    [SerializeField]
    private TMP_Text m_Text;
    [field: SerializeField]
    public UnityEvent OnMenuSubmit { get; private set; }

    private Button m_Button;
    
    // Start is called before the first frame update
    void Awake()
    {
        m_Button = gameObject.GetComponent<Button>();
        
        if (!m_Button) 
        {
            throw new System.Exception("A text field is necessary!");
        }    
    }

    // Update is called once per frame
    void Update()
    {
        m_Text.color = m_NormalColor;
        
        if (!m_Button.interactable) 
        {
            m_Text.color = m_DisabledColor;
        }
        else if (highlight)
        {
            m_Text.color = m_HighlightColor;
        }
    }
}
